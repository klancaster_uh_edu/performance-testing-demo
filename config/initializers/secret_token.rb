# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
PerfTestDemo::Application.config.secret_key_base = '4ddb1e50c26ef12f9fa186fbecbd32851754ccca9a266fc9a029b024414b9e0fbd95479db3da1eab6a9ddd07f7aa31093cfd61f94ab6d0546fd50820739923dd'
